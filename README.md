# avail-crypto

Total time: 3 hours or so.
The bulk of the time was analyzing and design, choice of libraries and then some time with actual coding and tests.

Libraries considered:
javax crypto package : Default Java SDK since 7 has crypto classes like Cipher for cipher blocks, KeyPairGenerator for public key/secret. Has both encrypt/decrypt.

Google Tink: uses AES/GCM. Higher level similar to Jasypt.

Spring security: This is an excellent library and can be used for high level purposes and Encryptors class is starting point.

Jasypt: can be integrated with spring. Higher level compared to javax crypto package.

Bouncy castle: Leading JCE provider that is also used in Spring Bcrypt and other classes. 

Question:
After you are done, return it and can you think of a reason I would ever do something like this?
Phrases are commonly used to identify a person or in lieu or passwords or for retrieving a forgotten password or any mechanism in the user identification (whether online of offline) process. Typically, one way could solve a lot of use cases though in certain cases like customer services where there is a human verification involved, it is acceptable to decrypt - in that case use a simpler algorithm but safeguard processes such as payment process or user identification using stronger algorithms.
