package com.avail.crypto.tests;

import java.security.GeneralSecurityException;
import java.util.List;

import com.avail.crypto.services.Encrypter;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class EncrypterTest 
    extends TestCase
{
	
	Encrypter encrypter = new Encrypter(); 
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public EncrypterTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( EncrypterTest.class );
    }

    /**
     * Rigourous Test :-)
     * @throws GeneralSecurityException 
     */
    public void testEncryptPhrase() throws GeneralSecurityException
    {
    	List<String> results = encrypter.encryptPhrase("Disney plus has incredibles and nemo");
    	
        assertTrue( results.size() == 1 );
    }

    public void testEncryptPhrase2() throws GeneralSecurityException
    {
    	List<String> results = encrypter.encryptPhrase("Disney plus has incredibles and nemo too");
    	
        assertTrue( results.size() == 1 );
    }
// for length 8     
//    public void testEncryptPhrase3() throws GeneralSecurityException
//    {
//    	List<String> results = encrypter.encryptPhrase("Disney plus has incredibles and finding nemo too");
//    	
//        assertTrue( results.size() == 1 );
//    }

    public void testEncryptPhraseForNull() throws GeneralSecurityException
    {
    	List<String> results = encrypter.encryptPhrase(null);
    	
        assertTrue( results == null );
    }
    
    public void testEncryptPhraseForEmpty() throws GeneralSecurityException
    {
    	List<String> results = encrypter.encryptPhrase("");
    	
        assertTrue( results == null );
    }
    
    public void testEncryptPhraseForPunctuation() throws GeneralSecurityException
    {
    	List<String> results = encrypter.encryptPhrase("Disney plus ! has incredibles and nemo");
    	
        assertTrue( results == null );
    }
}
