package com.avail.crypto.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	public static boolean checkPunctuation(String input) {
		Pattern pattern = Pattern.compile("\\p{Punct}");
		Matcher matcher = pattern.matcher(input);
		return matcher.find();
	}
	
}
