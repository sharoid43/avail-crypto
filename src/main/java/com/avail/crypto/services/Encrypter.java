package com.avail.crypto.services;

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.avail.crypto.utils.CryptoUtils;
import com.avail.crypto.utils.StringUtils;

/*
 * Main encrypter class that will encrypt a given phrase, merge till it has one output encryped phrase
 */
public class Encrypter {
	
	private final String salt = "Phrase";

	public List<String> encryptPhrase(String input) throws GeneralSecurityException {
		//input is null or ""
		if (input == null || "".equals(input))
			return null;
		//input has no punctuation. 
		if (StringUtils.checkPunctuation(input))
			return null;
		System.out.println("input = " + input);
		ArrayList<String> encryptedInputList = new ArrayList<String>();
		
		for (String str: input.split(" ")) {
			encryptedInputList.add(CryptoUtils.encryptTink(str, salt));//TODO:also check for max length of 7 words ..
		}
		ArrayList<String> encryptedOutputList = mergeAndEncrypt(encryptedInputList);
		for (String str: encryptedOutputList)
			System.out.println("encrypted output = " + str);
		
		return encryptedOutputList;
	}

	private ArrayList<String> mergeAndEncrypt(ArrayList<String> encryptedInputList) throws GeneralSecurityException {
		if (encryptedInputList.size() == 1)
			return encryptedInputList;
		Iterator<String> iterator = encryptedInputList.iterator();
		ArrayList<String> encryptedOutputList = new ArrayList<String>();
		boolean merged = false;
		while (!merged) {
			String word1, word2, outputWord = null;
			if (iterator.hasNext()) 
			{
				word1 = iterator.next();
				if (iterator.hasNext())
				{
					word2 = iterator.next();
					outputWord = word1+word2;
				} else {
					outputWord = word1;
					merged = true;
				}
			} else {
				merged = true;
			}
			if (outputWord!=null)
				encryptedOutputList.add(CryptoUtils.encryptTink(outputWord,salt));
		}
		return mergeAndEncrypt(encryptedOutputList); 
	}
}
